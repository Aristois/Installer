# Aristois Installer

Installer for the [Aristois](https://aristois.net/) Utility Mod.

## License

The Aristois installer is licensed under the MIT license.