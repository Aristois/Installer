package me.deftware.installer.model;

public class Configuration {

    private boolean clean;
    private boolean forge;
    private String instance;

    public Configuration withClean(boolean state) {
        clean = state;
        return this;
    }

    public Configuration withForge(boolean state) {
        forge = state;
        return this;
    }

    public Configuration withInstance(String instance) {
        this.instance = instance;
        return this;
    }

    public boolean isClean() {
        return clean;
    }

    public boolean isForge() {
        return forge;
    }

    public String getInstance() {
        return instance;
    }
}
